/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */
/*global $, define */

(function () {
    'use strict';
    
    var jsonRequest = { "bucket": "dev-redeye-au", "prefix": "george", "filename": "test.doc", "region": "ap-southeast-2", "company": "Alacer" };
    
    
    $.ajax({
        type: "POST",
        //url: "http://10.0.99.123:8090/docScraper/metadata",
        url: "http://localhost:8090/docScraper/metadata",
        data: JSON.stringify(jsonRequest),
        success: function (json) {
            var pretty = JSON.stringify(json);
            console.log("JSON Data: " + pretty);
            $('#json-response').html(pretty);
        },
        dataType: 'json',
        contentType: 'application/json',
        processType: false
    });
    
}());







