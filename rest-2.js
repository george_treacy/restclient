var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8080/file-processor",
    "method": "POST",
    "headers": {
        "content-type": "application/json"
    },
    "processData": false,
    "data": "{ \"bucket\": \"dev-redeye-au\", \"prefix\": \"george\", \"filename\": \"AlacerDoc.doc\", \"region\": \"ap-southeast-2\", \"company\": \"Alacer\" }"
}

$.ajax(settings).done(function (response) {
    $('#json-response').html(JSON.stringify(response));
    console.log(response);
});